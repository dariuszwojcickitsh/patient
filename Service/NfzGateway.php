<?php


namespace Service;


use Model\BodyDimensions;

interface NfzGateway
{
    public function reportPatientBodyDimensions(BodyDimensions $bodyDimensions): void;
}
<?php


namespace Service;


class BodyReport
{
    /** @var float */
    public $height;

    /** @var float */
    public $waistWidth;

    /** @var float */
    public $chestWidth;

    /** @var float */
    public $footSize;

    public function __construct(float $height, float $waistWidth, float $chestWidth, float $footSize)
    {
        $this->height = $height;
        $this->waistWidth = $waistWidth;
        $this->chestWidth = $chestWidth;
        $this->footSize = $footSize;
    }


}
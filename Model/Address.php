<?php


namespace Model;


final class Address
{
    /** @var string */
    private $street;

    /** @var string */
    private $number;

    /** @var string */
    private $city;

    /** @var string */
    private $post;

    public function __construct(string $street, string $number, string $city, string $post)
    {
        $this->street = $street;
        $this->number = $number;
        $this->city = $city;
        $this->post = $post;
    }
}
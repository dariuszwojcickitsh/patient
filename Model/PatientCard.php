<?php


namespace Model;


final class PatientCard
{
    /** @var Address */
    private $address;

    /** @var Appearance */
    private $appearance;

    /** @var BodyDimensions */
    private $bodyDimensions;

    /** @var PersonalData */
    private $personalData;

    public function __construct(Address $address, Appearance $appearance, BodyDimensions $bodyDimensions, PersonalData $personalData)
    {
        $this->address = $address;
        $this->appearance = $appearance;
        $this->bodyDimensions = $bodyDimensions;
        $this->personalData = $personalData;
    }

}
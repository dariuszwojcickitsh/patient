<?php


namespace Model;


final class PersonalData
{
    /** @var string */
    private $name;

    /** @var string */
    private $lastName;

    /** @var integer */
    private $pesel;

    /**@var \DateTime */
    private $birthDate;

    /** @var string */
    private $sex;

    /** @var string */
    private $sexualOrientation;

    /** @var string */
    private $birthPlace;

    public function __construct(string $name, string $lastName, int $pesel, \DateTime $birthDate, string $sex, string $sexualOrientation, string $birthPlace)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->pesel = $pesel;
        $this->birthDate = $birthDate;
        $this->sex = $sex;
        $this->sexualOrientation = $sexualOrientation;
        $this->birthPlace = $birthPlace;
    }
}
<?php


namespace Model;


final class Appearance
{
    /** @var string */
    private $eyeColor;

    /** @var string */
    private $hairColor;

    /** @var string */
    private $race;

    public function __construct(string $eyeColor, string $hairColor, string $race)
    {
        $this->eyeColor = $eyeColor;
        $this->hairColor = $hairColor;
        $this->race = $race;
    }
}
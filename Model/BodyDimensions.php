<?php


namespace Model;


final class BodyDimensions
{
    /** @var float */
    private $height;

    /** @var float */
    private $waistWidth;

    /** @var float */
    private $chestWidth;

    /** @var float */
    private $footSize;

    /** @var float */
    private $weight;

    public function __construct(float $height, float $waistWidth, float $chestWidth, float $footSize, float $weight)
    {
        $this->height = $height;
        $this->waistWidth = $waistWidth;
        $this->chestWidth = $chestWidth;
        $this->footSize = $footSize;
        $this->weight = $weight;
    }

    public function bmi(): float
    {
        return $this->weight / pow($this->height, 2);
    }
}